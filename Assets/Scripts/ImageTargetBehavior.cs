﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ImageTargetBehavior : MonoBehaviour, ITrackableEventHandler {
	public GameObject tipsPanel;
    public GameObject caputurePanel;
	private TrackableBehaviour mTrackableBehaviour;

	// Use this for initialization
	void Start () {
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        tipsPanel.SetActive(true);
        caputurePanel.SetActive(false);

        if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
        if (tipsPanel == null)
            return;

		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			tipsPanel.SetActive (false);
            caputurePanel.SetActive(true);
		}
		else
		{
			tipsPanel.SetActive (true);
            caputurePanel.SetActive(false);
        }
	} 
	
	// Update is called once per frame
	void Update () {
		
	}
}
